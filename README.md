# API Document 
## json-server
[https://github.com/typicode/json-server](https://github.com/typicode/json-server)

### 1. Install
```
npm install -g json-server
```

### 2. Run 
```
json-server api/faker.js --routes api/route.json
```
### 3. Routes
```
GET /orders
```

## Swagger
- API 文件使用swagger 
- Swagger URL: [https://swagger.io/specification/](https://swagger.io/specification/)

### Dependency
```
swagger-codegen 2.3.0
```
### Generate Code
```
java -jar ../swagger-codegen/modules/swagger-codegen-cli/target/swagger-codegen-cli.jar generate -i api/api.yml -l typescript-angular -o src/client/
```
### How to use
```
constructor(private api: APIService) {

}
```

```
this.api.ordersGet().subscribe(data => {
      this.orders = data
});
```