var faker = require('faker');
var randomTimestamp = require('random-timestamps');

module.exports = () => {
    const data = {
        orders: [],
        orderItems: [],
        users: [],
        favorites: [],
        carts: [],
        products: [],
        rooms: [],
        blobs: [],
        posts: [],
    };
    for (let i = 0; i < 20; i++) {
        data.orders.push({
            id: i,
            serial: faker.random.uuid().substr(1,5),
            state: faker.random.arrayElement([-1, 0, 0, 1, 1]),
            created: faker.date.between('2016-01-01', '2017-07-30'),
            freight: faker.random.number(),
            total: faker.random.number(),
            grandTotal: faker.random.number(),
            shipping: faker.random.arrayElement(['hd', 'pis'])
        });
    }

    for (let i = 0; i < 5; i++) {
        data.orderItems.push({
            productName: faker.name.findName(),
            price: faker.random.number(),
            image: faker.image.food()
        });
    }

    for (let i = 0; i < 20; i++) {
        data.users.push({
            id: i,
            name: faker.name.findName(),
            username: i == 0 ? 'admin' : faker.name.findName(),
            bounus: faker.random.number(),
            verified: faker.random.arrayElement([1, 0]),
            picture: 'http://i.pravatar.cc/' + faker.random.number(1000)
        });
    }

    for (let i = 0; i < 5; i++) {
        data.favorites.push({
            name: faker.name.findName(),
            price: faker.random.number(),
            image: faker.image.food()
        });
    }

    for (let i = 0; i < 5; i++) {
        data.carts.push({
            cartItems: [{
                productName: faker.name.findName(),
                price: faker.random.number(),
                image: faker.image.food()
            },{
                productName: faker.name.findName(),
                price: faker.random.number(),
                image: faker.image.food()
            }],
            id: i,
            freight: faker.random.number(),
            total: faker.random.number(),
            grandTotal: faker.random.number(),
            recipientAddress: faker.address.streetAddress()
        });
    }

    for (let i = 0; i < 3; i++) {
        data.products.push({
            title: faker.commerce.productName(),
            price: faker.random.number(),
            memberPrice: faker.random.number(),
            image: faker.random.image(),
            introtext: faker.lorem.sentence()
        });
    }

    for (let i = 0; i < 3; i++) {
        data.rooms.push({
            title: faker.commerce.product(),
            url: faker.internet.url(),
            status: faker.random.boolean(),
            image: faker.image.technics(),
            audience: faker.random.number(),
            host: faker.name.findName(),
        });
    }

    data.blobs = [...Array(1000).keys()].map( (cnt) => {

        if ([591, 920].includes(cnt)) {
            return []
        }
        
        let origin_width = faker.random.arrayElement([640, 480])
        let origin_height = faker.random.arrayElement([640, 480])

        return {
            blob_id: 'https://picsum.photos/' + origin_width + '/' + origin_height + '/?image=' + String(cnt),
            origin_width: origin_width,
            origin_height: origin_height
        }
    });

    data.posts = [...Array(1000).keys()].map( (cnt) => {
        return {
            blob: faker.random.arrayElement(data.blobs),
            category_id: faker.random.number(),
            comment_count: faker.random.number(),
            content: faker.lorem.sentence(),
            country_id: faker.random.number(128),
            createtime: randomTimestamp(),
            dislike_count: faker.random.number(),
            like_count: faker.random.number(),
            post_id: faker.random.number(),
            type: faker.random.number(),
            updatetime: randomTimestamp(),
            user: faker.random.arrayElement(data.users),
        }
    });

    return data
}
